<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Terima extends Notification
{
    use Queueable;
    private $tempat;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tempat)
    {
        $this->tempat = $tempat;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pemberitahuan PKL')
                    ->from('banyu.rizaldi@gmail.com', 'Cyberlabs')
                    ->line('Assalamualaikum.')
                    ->line('Bagaimana kabarnya hari ini?.')
                    ->line('Kami ingin memberitahu bahwa Anda diterima untuk prakerin di Cyberlabs. Dan Anda akan ditempatkan pada bagian ' . $this->tempat .'')
                    ->line('Untuk selanjutnya silahkan menghubungi kami melalui email, atau datang langsung.')
                    ->line('Terima Kasih.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

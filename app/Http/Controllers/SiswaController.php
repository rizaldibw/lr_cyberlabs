<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Terima;
use App\Notifications\Tolak;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('cari')) {
            $data_siswa = \App\Siswa::where('nama', 'LIKE', '%' . $request->cari . '%')->get();
        } else {
            // $data_siswa = \App\Siswa::all();
            $data_siswa = \App\Siswa::where('status',1)->get();
        }
        return view('siswa.index', ['data_siswa' => $data_siswa]);
    }

    public function indexVerifikasi()
    {
        $data_siswa = \App\Siswa::where('status',0)->get();

        return view('siswa.verifikasi', ['data_siswa' => $data_siswa]);
    }

    public function create(Request $request)
    {
        // \App\Siswa::create($request->all());
        \App\Siswa::create([
            'nama' => $request->nama, 
            'nim' => $request->nim, 
            'institusi' => $request->institusi, 
            'jurusan' => $request->jurusan, 
            'tanggal_mulai' => $request->tanggal_mulai, 
            'tanggal_selesai' => $request->tanggal_selesai,
            'status' => 0,
            'penempatan' => "-",
            'email' => $request->email
        ]);
        // return redirect('/siswa')->with('sukses', 'Data Berhasil Diinput');
        return redirect('/pendaftaran')->with('sukses', 'Data Berhasil Diinput, tunggu verifikasi selanjutnya dari Admin.');
    }

    public function edit($id)
    {
        $siswa = \App\Siswa::find($id);
        return view('siswa/edit', ['siswa' => $siswa]);
    }

    public function update(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->update($request->all());
        return redirect('/siswa')->with('sukses', 'Data berhasil diupdate');
    }

    public function updateTerima(Request $request, $id)
    {
        $tempat = $request->tempat;
        $siswa = \App\Siswa::find($id);
        $siswa->update([
            'penempatan' => $tempat,
            'status' => 1
        ]);

        Notification::route('mail', $siswa->email)->notify(new Terima($tempat));
        return redirect('/verifikasi')->with('sukses', 'Status berhasil diupdate');
    }

    public function delete($id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->delete();
        return redirect('/siswa')->with('sukses', 'Data berhasil dihapus');
    }

    public function pendaftaran()
    {
        return view('siswa.pendaftaran');
    }

    public function laporan()
    {
        return view('siswa.laporan');
    }

    public function laporanPrakerin(Request $request)
    {
        $siswa = \App\Siswa::whereBetween('tanggal_mulai',[$request->tanggal_mulai,$request->tanggal_selesai])
                ->orWhereBetween('tanggal_selesai',[$request->tanggal_mulai,$request->tanggal_selesai])
                ->get();
        // return $siswa;
        $awal = $request->tanggal_mulai;
        $selesai = $request->tanggal_selesai;

        $pdf = PDF::loadView('siswa.pdf',compact('siswa','awal','selesai'));
        return $pdf->stream();
    }

    public function destroy($id)
    {
        $siswa = \App\Siswa::find($id);
        Notification::route('mail', $siswa->email)->notify(new Tolak());

        $siswa->delete();

        return redirect('/verifikasi')->with('sukses','Data berhasil dihapus.');
        // dd($siswa);
    }

    // public function email()
    // {
    //     $emailAdmin = "170914004@fellow.lpkia.ac.id";
    //     $luthfi = "Luthfi";
    //     Notification::route('mail', $emailAdmin)->notify(new Terima($luthfi));
    //     return "berhasil";
        // $siswa = \App\Siswa::find($id);
        // $siswa->delete();

        // return redirect('/verifikasi')->with('sukses','Data berhasil dihapus.');
        // dd($siswa);
    // }
}

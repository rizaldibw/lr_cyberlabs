
@extends('layouts.login.master')
{{-- @extends('layouts.app') --}}

@section('title','Laporan Data Siswa Prakerin')

@section('content')

<div class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TABLE HOVER -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3>LAPORAN DATA SISWA PRAKERIN</h3>
                    </div>
                    <div class="panel-body">
                        <form action="/laporanPrakerin" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-sm-2">
                                    <label for="tanggal_mulai">Tanggal Awal</label>
                                    <input type="date" name="tanggal_mulai" id="tanggal_mulai" class="form-control">
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row"> --}}
                                <div class="col-sm-2">
                                    <label for="tanggal_selesai">Tanggal Akhir</label>
                                    <input type="date" name="tanggal_selesai" id="tanggal_selesai" class="form-control">
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="modal-footer"> --}}
                                {{-- <div class="col-sm-2"> --}}
                                    <input type="submit" name="submit" class="btn btn-info" value="Cetak">
                                {{-- </div> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
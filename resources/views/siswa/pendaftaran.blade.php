@extends('layouts.login.master')

@section('title','Data Siswa Prakerin')

@section('content')

@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{session('sukses')}}
</div>
@endif
<div class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TABLE HOVER -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3>
                            FORM PENDAFTARAN SISWA PRAKERIN
                        </h3>
                        {{--
                        <div class="right">
                            <button class="btn float-right" data-target="#exampleModal" data-toggle="modal" type="button">
                                Tambah Peserta Prakerin
                                <i class="lnr lnr-plus-circle">
                                </i>
                            </button>
                        </div>
                        --}}
                    </div>
                    <div class="panel-body">
                        <form action="/siswa/create" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Nama Lengkap
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="nama" placeholder="Nama Lengkap" type="text">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    NIM/NIS
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="nim" placeholder="NIM/NIS" type="text">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Institusi
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="institusi" placeholder="Institusi" type="text">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Jurusan
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="jurusan" placeholder="Jurusan" type="text">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Email
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" type="email">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Tanggal Mulai
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="tanggal_mulai" readonly="" type="text" value="{{$data_siswa = date('Y-m-d')}}">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    Tanggal Selesai
                                </label>
                                <input class="form-control" id="exampleInputEmail1" name="tanggal_selesai" type="date">
                                </input>
                            </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <button class="btn btn-secondary" data-dismiss="modal" type="button">
                            Close
                        </button> --}}
                        <button class="btn btn-primary" type="submit">
                            Daftar
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{--
<table class="table table-hover">
    <thead>
        <tr>
            <th>
                Nama Lengkap
            </th>
            <th>
                NIM/NIS
            </th>
            <th>
                Institusi
            </th>
            <th>
                Jurusan
            </th>
            <th>
                Tanggal Mulai
            </th>
            <th>
                Tanggal Selesai
            </th>
            <th>
                Aksi
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data_siswa as $siswa)
        <tr>
            <td>
                {{$siswa->nama}}
            </td>
            <td>
                {{$siswa->nim}}
            </td>
            <td>
                {{$siswa->institusi}}
            </td>
            <td>
                {{$siswa->jurusan}}
            </td>
            <td>
                {{$siswa->tanggal_mulai}}
            </td>
            <td>
                {{$siswa->tanggal_selesai}}
            </td>
            <td>
                <a class="btn btn-warning btn-sm" href="siswa/{{$siswa->id}}/edit">
                    Edit
                </a>
                <a class="btn btn-danger btn-sm" href="siswa/{{$siswa->id}}/delete" onclick="return confirm('Yakin mau dihapus ?')">
                    Delete
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
--}}
{{-- <div> --}}
    <!-- END TABLE HOVER -->
    
{{-- </div> --}}
<!-- Modal -->
{{-- <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Data Siswa Prakerin
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body"> --}}
                {{--
                <form action="/siswa/create" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Nama Lengkap
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="nama" placeholder="Nama Lengkap" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            NIM/NIS
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="nim" placeholder="NIM/NIS" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Institusi
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="institusi" placeholder="Institusi" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Jurusan
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="jurusan" placeholder="Jurusan" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Tanggal Mulai
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="tanggal_mulai" readonly="" type="text" value="{{$data_siswa = date('Y-m-d')}}">
                        </input>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            Tanggal Selesai
                        </label>
                        <input class="form-control" id="exampleInputEmail1" name="tanggal_selesai" type="date">
                        </input>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button">
                    Close
                </button>
                <button class="btn btn-primary" type="submit">
                    submit
                </button>
            </div>
        </div>
    </div>
</div>
--}}
@endsection

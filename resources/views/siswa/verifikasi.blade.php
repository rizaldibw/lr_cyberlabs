@extends('layouts.login.master')
{{-- @extends('layouts.app') --}}

@section('title','Verifikasi Siswa Prakerin')

@section('content')

@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{session('sukses')}}
</div>
@endif

<div class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TABLE HOVER -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3>VERIFIKASI SISWA PRAKERIN</h3>
                        {{-- <div class="right">
                            <button type="button" class="btn float-right" data-toggle="modal" data-target="#exampleModal">
                                Tambah Peserta Prakerin<i class="lnr lnr-plus-circle"></i>
                            </button>
                        </div> --}}
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <th>NIM/NIS</th>
                                    <th>Institusi</th>
                                    <th>Jurusan</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_siswa as $siswa)
                                <tr>
                                    <td>{{$siswa->nama}}</td>
                                    <td>{{$siswa->nim}}</td>
                                    <td>{{$siswa->institusi}}</td>
                                    <td>{{$siswa->jurusan}}</td>
                                    <td>{{$siswa->tanggal_mulai}}</td>
                                    <td>{{$siswa->tanggal_selesai}}</td>
                                    <td>
                                        <form>
                                            {{-- <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#exampleModal">
                                                Terima
                                            </button> --}}
                                            <a href="javascript:;" data-toggle="modal" class="btn btn-warning btn-sm" onclick="deleteData({{ $siswa->id }})" data-target="#DeleteModal">Terima</a>
                                            <a href="javascript:;" data-toggle="modal" class="btn btn-danger btn-sm" onclick="hapusData({{ $siswa->id }})" data-target="#hapusModal">Tolak</a>
                                            {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">
                                                Tolak
                                            </button> --}}
                                        </form>
                                        {{-- <a href="siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm">Terima</a> --}}
                                        {{-- <a href="siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus ?')">Tolak</a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div>
                    <!-- END TABLE HOVER --> -->
                </div>
            </div>
        </div>
    </div>
</div>


{{-- terima --}}
<div id="DeleteModal" class="modal fade" role="dialog">
 <div class="modal-dialog ">
   <!-- Modal content-->
   <form action="" id="deleteForm" method="POST">
        @csrf
        @method('PUT')
       <div class="modal-content">
           <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Terima</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title text-center">Konfirmasi Terima</h4> --}}
         </div>
         <div class="modal-body">
            <label for="tempat">Penempatan</label>
            <select name="tempat" id="tempat" class="form-control">
                <option value="IT Support">IT Support</option>
                <option value="Designer">Designer</option>
                <option value="Team Developer">Team Developer</option>
            </select>
             {{-- <p class="text-center">Anda yakin untuk menerimanya ?</p> --}}
         </div>
         <div class="modal-footer">
             <center>
                 <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                 <input type="submit" name="" value="Yes" class="btn btn-danger">
                 {{-- <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button> --}}
               </center>
           </div>
       </div>
   </form>
 </div>
</div>

{{-- tolak --}}
<div id="hapusModal" class="modal fade" role="dialog">
 <div class="modal-dialog ">
   <!-- Modal content-->
   <form action="" id="deletePendaftar" method="POST">
        @csrf
        @method('DELETE')
       <div class="modal-content">
           <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Tolak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title text-center">Konfirmasi Terima</h4> --}}
         </div>
         <div class="modal-body">
             <p class="text-center">Anda yakin untuk menolaknya ?</p>
         </div>
         <div class="modal-footer">
             <center>
                 <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                 <input type="submit" name="" value="Yes" class="btn btn-danger">
                 {{-- <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Yes, Delete</button> --}}
               </center>
           </div>
       </div>
   </form>
 </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Siswa Prakerin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/siswa/create" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lengkap</label>
                        <input name="nama" type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Lengkap">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">NIM/NIS</label>
                        <input name="nim" type="text" class="form-control" id="exampleInputEmail1" placeholder="NIM/NIS">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Institusi</label>
                        <input name="institusi" type="text" class="form-control" id="exampleInputEmail1" placeholder="Institusi">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Jurusan</label>
                        <input name="jurusan" type="text" class="form-control" id="exampleInputEmail1" placeholder="Jurusan">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input name="tanggal_mulai" type="text" class="form-control" id="exampleInputEmail1" value="{{$data_siswa = date('Y-m-d')}}" readonly>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input name="tanggal_selesai" type="date" class="form-control" id="exampleInputEmail1">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    function deleteData(id)
    {
        var id = id;
        var url = '/verifikasi/'+id;
        // url = url.replace(':id', id);
        $("#deleteForm").attr('action', url);
    }

    function hapusData(id)
    {
        var id = id;
        var url = '/verifikasi/'+id;
        // url = url.replace(':id', id);
        $("#deletePendaftar").attr('action', url);
    }

   // function formSubmit()
   // {
   //     $("#deleteForm").submit();
   // }
</script>
@endsection
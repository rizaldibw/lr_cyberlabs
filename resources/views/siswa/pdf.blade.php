<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2><center>Laporan Data Prakerin</center></h2>
	<p style="margin-left: 1cm">Antara tanggal {{ date('d-m-Y',strtotime($awal)) }} sampai tanggal {{ date('d-m-Y', strtotime($selesai)) }}</p>
	<table border="1" align="center">
		<tr>
			<th style="width: 5%">No</th>
			<th style="width: 15%">Nama</th>
			<th style="width: 10%">NIM</th>
			<th style="width: 15%">Institusi</th>
			<th style="width: 15%">Jurusan</th>
			<th style="width: 10%">Tanggal Mulai</th>
			<th style="width: 10%">Tanggal Selesai</th>
			<th style="width: 20%">Penempatan</th>
		</tr>
		@foreach($siswa as $data)
		<tr>
			<td style="text-align: center;">{{ $loop->iteration }}</td>
			<td>{{ $data->nama }}</td>
			<td>{{ $data->nim }}</td>
			<td>{{ $data->institusi }}</td>
			<td>{{ $data->jurusan }}</td>
			<td>{{ date('d-m-Y', strtotime($data->tanggal_mulai)) }}</td>
			<td>{{ date('d-m-Y', strtotime($data->tanggal_selesai)) }}</td>
			<td>{{ $data->penempatan }}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>